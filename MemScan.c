/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab03a - memory scanner - EE 491 - Spr 2022
///
/// @file    MemoryScanner.c
/// @version 1.0 - Initial version
///
/// Memory Scanner - Parse and displays contents of /proc/self/maps
///
/// @author   Duncan Lajousky <lajousky@hawaii.edu>
/// @date     03_02_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "MemScan.h"

//scanning and parsing structure//
int scanner(Data Mapsdata, int lines){
   int bytes=0, As=0;

   if (strstr (Mapsdata.permissions, "r") !=NULL){
         char* currentadd = Mapsdata.startadd;
         char* finadd = Mapsdata.endadd;
         //couting A and bytes//
         while(currentadd !=finadd){
            if(*currentadd == 'A') {
               As +=1; }
               bytes +=1;
               currentadd = currentadd +1;
               }
          }
  
          printf("%d: %s-%s %s Number of bytes read [%d} Number of 'A' is[%d]\n",lines,Mapsdata.startadd,Mapsdata.endadd,Mapsdata.permissions,bytes,As);

   return 0;
   }

//opening /proc/self/maps//
int main(int argc, char* argv[]){
//unexpected file//
   if (argc !=2){
      fprintf(stderr, "UsageERR\n", FILENAME);
      exit(EXIT_FAILURE);
   }else if( !( strcmp( argv[1], "/proc/self/maps" ) == 0 ) ){
    fprintf(stderr, "UsageERR\n", FILENAME);
    exit(EXIT_FAILURE);
    }

   FILE* fp=NULL;
   fp = fopen(argv[1], "r");
///proc/self/maps opens properly start parse//
      int lines = 0;
      size_t length=0;

      //creating data structure//
      Data Mapsdata = {" ", " ", " "};
      while ((read = getline(&line, &length, fp)) != -1 ){
 
         //defining address line//
      
         address = strtok(line, " ");
         permissions = strtok(NULL, " ");   
         //splitting address line in half//
         addstart = strtok(address, "-");
         addend = strtok(NULL, "-");

         //finding permissions section//
   

         strcpy(Mapsdata.startadd, addstart);
         strcpy(Mapsdata.endadd, addend);
         strcpy(Mapsdata.permissions, permissions);
        
         scanner(Mapsdata, lines);
         lines++;

      }
      fclose(fp);
      return 0;

}




