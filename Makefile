###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 03a - Memory Scanner - EE 491 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a Memory Scanner
###
### @author  Duncan Lajousky <lajousky@hawaii.edu>
### @date    03_02_2022
###
###############################################################################

CC= gcc
CFLAGS= -g -Wall

TARGET= MemScan

all: $(TARGET)

MemScan: MemScan.c
				$(CC) $(CFLAGS) -o $(TARGET) MemScan.c

clean: rm -f $(TARGET)

test: ./MemScan /prod/self/maps
