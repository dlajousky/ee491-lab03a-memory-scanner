
/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab03a - memory scanner - EE 491 - Spr 2022
///
/// @file    MemoryScanner.h
/// @version 1.0 - Initial version
///
/// Memory Scanner - Parse and displays contents of /proc/self/maps
///
/// @author   Duncan Lajousky <lajousky@hawaii.edu>
/// @date     03_02_2022
///////////////////////////////////////////////////////////////////////////////
#define FILENAME "MemScan"
#define MAX 1000
char* line;
char* address;
char* addstart;
char* addend;
char* now;
char* permissions;
ssize_t read;

typedef struct {
   char startadd[MAX];
   char endadd[MAX];
   char permissions[10];
   int bytes;
   int As;
} Data;

